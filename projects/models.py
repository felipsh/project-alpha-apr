from django.contrib.auth.models import User
from django.db import models
from tasks.models import Task


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        User, related_name="projects", on_delete=models.CASCADE, null=True
    )
    task = models.ForeignKey(
        Task,
        null=True,
        related_name="tasks",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


# Create your models here.
